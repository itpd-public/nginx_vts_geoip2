FROM centos:centos8 as build
RUN cd /root && yum install rpmdevtools wget gcc git unzip redhat-lsb-core openssl-devel zlib-devel pcre-devel libmaxminddb-devel -y && \
    rpm -Uhv https://nginx.org/packages/mainline/centos/8/SRPMS/nginx-1.19.9-1.el8.ngx.src.rpm && \
    git clone https://github.com/leev/ngx_http_geoip2_module && \
    git clone https://github.com/vozlt/nginx-module-vts && \
    sed -i '72s|")| --add-module=/root/ngx_http_geoip2_module --add-module=/root/nginx-module-vts")|' /root/rpmbuild/SPECS/nginx.spec && \
    rpmbuild -bb /root/rpmbuild/SPECS/nginx.spec

FROM centos:centos8
COPY --from=build /root/rpmbuild/RPMS/x86_64/nginx-1.19.9-1.el8.ngx.x86_64.rpm /tmp
COPY GeoIP2-Country.mmdb /usr/share/GeoIP/GeoIP2-Country.mmdb

RUN yum install -y wget openssl sed gettext libmaxminddb &&\
    yum -y autoremove &&\
    yum clean all &&\
    rpm -ihv /tmp/nginx-1.19.9-1.el8.ngx.x86_64.rpm && \
    rm -rf /tmp/nginx-1.19.9-1.el8.ngx.x86_64.rpm && \
    chown -R nginx:nginx /var/cache/nginx && \
    ln -sf /dev/stdout /var/log/nginx/access.log && ln -sf /dev/stderr /var/log/nginx/error.log

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
